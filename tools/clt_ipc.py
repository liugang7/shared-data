from multiprocessing.connection import Client

def tryx(l,e=print):
    try: return l()
    except Exception as ex: return ex if True==e else e(ex) if e else None

from time import time as now

import sys

from sys import argv
argc = len(argv)

#from mypy import tryx, now, parallel, argv, argc, sys

port = tryx(lambda:int(argv[1]),False)
if port is None:
  address = f'\\\\.\\Pipe\\{argv[1]}' if sys.platform=='win32' else f'../tmp/{argv[1]}.sck'
else:
  host = tryx(lambda:argv[2],False) or '127.0.0.1'
  address = (host,port)

print(address)

def send_once(v):
  conn = Client(address)
  conn.send(v)
  rt = conn.recv()
  conn.close()
  return rt

def yield_arg():
  for v in range(1,9999):
    yield v

for line in sys.stdin: print(tryx(lambda:send_once(line)))

"""
e.g.

> python clt_ipc 16666 127.0.0.1

or

> python clt_ipc qmtipc

# 列表
get_stock_list_in_sector('沪深A股')
len(get_stock_list_in_sector('沪深A股'))

# 我的持仓
my_pos()

# 拉价格
pull_price()


"""




